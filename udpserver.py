import socket
from timesync import *
import argparse

def run(HOST = "127.0.0.1", PORT=37):
    print("Starting UDP time server")
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.bind((HOST, PORT))
        sock.settimeout(2)

        while True:
            try:
                data, addr = sock.recvfrom(1024)
                print(f"Received {data} from {addr}")
                if data == b"":
                    t = get_timestamp()
                    tx_data = int_to_bytes(t)
                    print(f"Sending time {t} as {tx_data}")
                    sock.sendto(tx_data, addr)
                else:
                    print("Expected empty datagram. Received {data}")
            except socket.timeout:
                pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser("RFC868 UDP Time server")
    parser.add_argument('--host', '-H', default="0.0.0.0")
    parser.add_argument('port', type=int)

    args = parser.parse_args()
    try:
        run(args.host, args.port)
    except KeyboardInterrupt:
        print("Time protocol server stopped")