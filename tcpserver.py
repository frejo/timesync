import socket
from timesync import *
import argparse

def run(HOST = "127.0.0.1", PORT=37):
    print("Starting TCP time server")
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((HOST, PORT))
        sock.settimeout(2)
        sock.listen()

        while True:
            try:
                conn, addr = sock.accept()

                print(f"Connected to {addr}")

                t = get_timestamp()
                tx_data = int_to_bytes(t)
                print(f"Sending time {t} as {tx_data}")

                conn.sendall(tx_data)
            except socket.timeout:
                pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser("RFC868 TCP Time server")
    parser.add_argument('--host', '-H', default="0.0.0.0")
    parser.add_argument('port', type=int)

    args = parser.parse_args()
    try:
        run(args.host, args.port)
    except KeyboardInterrupt:
        print("Time protocol server stopped")