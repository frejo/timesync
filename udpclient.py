import socket
from timesync import *
import argparse

def run(HOST, PORT):
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
        sock.settimeout(5)
        sock.sendto(b"", (HOST, PORT))
        data, addr = sock.recvfrom(1024)
        t = bytes_to_int(data)
        print(f"Received raw data: {data}")
        print(f"received HEX data: {data.hex()}")
        print(f"Received timestamp {t}")
        print(f"Date: {read_timestamp(t)}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser("RFC868 UDP Time Client")
    parser.add_argument('host')
    parser.add_argument('port', type=int)

    args = parser.parse_args()
    run(args.host, args.port)