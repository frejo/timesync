# Time Sync

## Usage
Clients can be run by calling
```
python3 [tcp/udp]client.py [HOST] [PORT]
```

Run the servers by calling
```
python3 [tcp/udp]server.py [PORT]
```
