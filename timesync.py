from datetime import datetime, timedelta

_origin = datetime(1900, 1, 1)

def get_timestamp():
    delta = datetime.utcnow() - _origin
    return int(delta.total_seconds())

def read_timestamp(ts:int):
    time = _origin + timedelta(0, ts)
    return time

def int_to_bytes(data:int):
    return data.to_bytes(4, 'big')

def bytes_to_int(data:bytes):
    return int.from_bytes(data, 'big')
